package com.curiouslabs.groups.service

import javax.inject.Inject

import com.curiouslabs.groups.error.GroupServiceError
import com.curiouslabs.groups.model.Visibility.Visibility
import com.curiouslabs.groups.model._
import com.curiouslabs.groups.repository.GroupRepository

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by visitor15 on 11/7/16.
  */
class GroupService @Inject()(groupRepository: GroupRepository)(implicit ec: ExecutionContext) extends CrudService[Group, GroupCreationRequest, GroupReadRequest, GroupUpdateRequest, GroupDeleteRequest, GroupServiceError] {
  override def create(createRequest: GroupCreationRequest): Future[Either[GroupServiceError, Group]] = {
    groupRepository.createGroup(createRequest).map { group =>
      group match {
        case Some(g)  => Right(g)
        case None     => Left(GroupServiceError(500, "Unable to create group"))
      }
    }
  }

  override def read(readRequest: GroupReadRequest): Future[Either[GroupServiceError, Group]] = {
    readRequest.id match {
      case Some(groupId)  => {
        groupRepository.findGroupById(groupId).map { optGroup =>
          optGroup match {
            case Some(g)  => Right(g)
            case None     => Left(GroupServiceError(404, s"Group $groupId not found"))
          }
        }
      }
      case None           => Future.successful(Left(GroupServiceError(404, "No group ID provided")))
    }
  }

  override def update(updateRequest: GroupUpdateRequest): Future[Either[GroupServiceError, Group]] = {
    Future.successful(Left(GroupServiceError(500, "Unsupported action")))
  }

  override def delete(deleteRequest: GroupDeleteRequest): Future[Either[GroupServiceError, Group]] = {
    Future.successful(Left(GroupServiceError(500, "Unsupported action")))
  }

  def findNearest(readRequest: GroupReadRequest): Future[Either[GroupServiceError, List[Group]]] = {
    groupRepository.findNearest(readRequest).map { optGroups =>
      optGroups match {
        case Some(groups) => Right(groups)
        case None         => Left(GroupServiceError(404, "No groups found for given location"))
      }
    }
  }
}

case class GroupCreationRequest(location: Location,
                                title: String,
                                description: String,
                                visibility: Visibility,
                                founders: Set[Reference],
                                members: Option[Set[Reference]]) extends CreateRequest

case class GroupReadRequest(id:           Option[String],
                            location:     Option[Location],
                            minDistance:  Option[Long],
                            maxDistance:  Option[Long],
                            limit:        Option[Long],
                            skip:         Option[Long]) extends ReadRequest

case class GroupUpdateRequest(id: String) extends UpdateRequest

case class GroupDeleteRequest(id: String) extends DeleteRequest