package com.curiouslabs.groups.service

import javax.inject.Inject

import com.curiouslabs.groups.error.UserProfileServiceError
import com.curiouslabs.groups.model.{Group, ReferenceFactory, UserProfile}
import com.curiouslabs.groups.repository.{GroupRepository, UserProfileRepository}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by visitor15 on 11/7/16.
  */
class UserProfileService @Inject()(userRepository: UserProfileRepository, groupRepository: GroupRepository)(implicit ec: ExecutionContext) extends CrudService[UserProfile, UserProfileCreationRequest, UserProfileReadRequest, UserProfileUpdateRequest, UserProfileDeleteRequest, UserProfileServiceError] {
  override def create(createRequest: UserProfileCreationRequest): Future[Either[UserProfileServiceError, UserProfile]] = {
    val user = UserProfile.fromCreationRequest(createRequest)
    userRepository.saveUser(user).map { writeResult =>
      if(writeResult.ok) Right(user) else Left(UserProfileServiceError(500, "Unable to create user"))
    }
  }

  override def read(readRequest: UserProfileReadRequest): Future[Either[UserProfileServiceError, UserProfile]] = {
    userRepository.findUserById(readRequest.id).map { optUser =>
      optUser match {
        case Some(u)  => Right(u)
        case None     => Left(UserProfileServiceError(404, s"User ${readRequest.id} not found"))
      }
    }
  }

  override def update(updateRequest: UserProfileUpdateRequest): Future[Either[UserProfileServiceError, UserProfile]] = {
    Future.successful(Left(UserProfileServiceError(500, "Unsupported action")))
  }

  override def delete(deleteRequest: UserProfileDeleteRequest): Future[Either[UserProfileServiceError, UserProfile]] = {
    Future.successful(Left(UserProfileServiceError(500, "Unsupported action")))
  }

  def update(user: UserProfile): Future[Either[UserProfileServiceError, UserProfile]] = {
    userRepository.saveUser(user).map { writeResult =>
      if (writeResult.ok) Right(user) else Left(UserProfileServiceError(500, s"Error updating user ${user.id}"))
    }
  }

  def joinGroup(id: String, groupId: String): Future[Either[UserProfileServiceError, UserProfile]] = {
    val joinResult = for {
      user  <- userRepository.findUserById(id)
      group <- groupRepository.findGroupById(groupId)
    } yield {
      (user, group) match {
        case (None, None)       => Future.successful(Left(UserProfileServiceError(404, s"User $id and group $groupId not found")))
        case (None, Some(g))    => Future.successful(Left(UserProfileServiceError(404, s"User $id not found")))
        case (Some(u), None)    => Future.successful(Left(UserProfileServiceError(404, s"Group $groupId not found")))
        case (Some(u), Some(g)) => groupRepository.addUser(u.id, g).flatMap { result =>
          result match {
            case Left(e)  => Future.successful(Left(UserProfileServiceError(e.code, e.message)))
            case Right(g) => update(u.addGroup(ReferenceFactory.createGroupReference(g.id)))
          }
        }
      }
    }
    joinResult.flatMap(identity)
  }

  def leaveGroup(id: String, groupId: String): Future[Either[UserProfileServiceError, UserProfile]] = {
    val leaveResult = for {
      user <- userRepository.findUserById(id)
      group <- groupRepository.findGroupById(groupId)
    } yield {
      (user, group) match {
        case (None, None)       => Future.successful(Left(UserProfileServiceError(404, s"User $id and group $groupId not found")))
        case (None, Some(g))    => Future.successful(Left(UserProfileServiceError(404, s"User $id not found")))
        case (Some(u), None)    => Future.successful(Left(UserProfileServiceError(404, s"Group $groupId not found")))
        case (Some(u), Some(g)) => groupRepository.removeUser(u.id, g).flatMap { result =>
          result match {
            case Left(e)  => Future.successful(Left(UserProfileServiceError(e.code, e.message)))
            case Right(g) => update(u.removeGroup(ReferenceFactory.createGroupReference(g.id)))
          }
        }
      }
    }
    leaveResult.flatMap(identity)
  }

  def findJoinedGroups(id: String): Future[Either[UserProfileServiceError, List[Group]]] = {
    val joinedGroups = userRepository.findUserById(id).map { optUser =>
      optUser match {
        case None     => Future.successful(Left(UserProfileServiceError(404, s"User $id not found")))
        case Some(u)  => {
          u.groups match {
            case None         => Future.successful(Left(UserProfileServiceError(412, s"User $id has not joined any groups")))
            case Some(grpSet) => groupRepository.findGroupsById(grpSet.map(ref => ref.id)).map { result =>
              if (result.isEmpty) Left(UserProfileServiceError(404, s"User $id has not joined any groups")) else Right(result)
            }
          }
        }
      }
    }
    joinedGroups.flatMap(identity)
  }
}

case class UserProfileCreationRequest(email:       String,
                                      password:    String,
                                      firstName:   String,
                                      lastName:    String,
                                      displayName: Option[String],
                                      id:          Option[String]) extends CreateRequest

case class UserProfileReadRequest(id: String) extends ReadRequest

case class UserProfileUpdateRequest(id: String,
                                    email: Option[String],
                                    firstName: Option[String],
                                    lastName: Option[String],
                                    displayName: Option[String]) extends UpdateRequest

case class UserProfileDeleteRequest(id: String) extends DeleteRequest