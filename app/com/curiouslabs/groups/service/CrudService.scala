package com.curiouslabs.groups.service

import com.curiouslabs.groups.error.ServiceError
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.BSONDocumentReader

import scala.concurrent.Future


/**
  * Created by visitor15 on 11/7/16.
  */
trait CrudService[T, C <: CreateRequest, R <: ReadRequest, U <: UpdateRequest, D <: DeleteRequest, E <: ServiceError] {
  def create(createRequest: C): Future[Either[E, T]]
  def read(readRequest: R): Future[Either[E, T]]
  def update(updateRequest: U): Future[Either[E, T]]
  def delete(deleteRequest: D): Future[Either[E, T]]
}

trait UpdatedCrudService {
  def readById[T](id: String, collection: BSONCollection)(implicit bSONDocumentReader: BSONDocumentReader[T]) = {

  }
}

trait CreateRequest
trait ReadRequest
trait UpdateRequest
trait DeleteRequest