package com.curiouslabs.groups

import com.curiouslabs.authenticator.repository.{AuthRepository, AuthenticatorDBConfiguration, UserCredentialRepository}
import com.curiouslabs.authenticator.service.{AuthenticationActionService, AuthenticationService, RegistrationService, UserService}
import com.curiouslabs.groups.repository.{GroupRepository, UserProfileRepository}
import com.curiouslabs.groups.service.{GroupService, UserProfileService}
import com.curiouslabs.mongo.DefaultMongoConfiguration
import com.google.inject.AbstractModule

/**
  * Created by visitor15 on 11/7/16.
  */
class Module extends AbstractModule{
  override def configure() = {
    bind(classOf[DefaultMongoConfiguration]).asEagerSingleton()
    bind(classOf[AuthenticatorDBConfiguration]).asEagerSingleton()
    bind(classOf[AuthRepository]).asEagerSingleton()
    bind(classOf[UserCredentialRepository]).asEagerSingleton()
    bind(classOf[UserProfileRepository]).asEagerSingleton()
    bind(classOf[GroupRepository]).asEagerSingleton()
    bind(classOf[RegistrationService]).asEagerSingleton()
    bind(classOf[UserProfileService]).asEagerSingleton()
    bind(classOf[GroupService]).asEagerSingleton()
    bind(classOf[UserService]).asEagerSingleton()
    bind(classOf[AuthenticationActionService]).asEagerSingleton()
    bind(classOf[AuthenticationService]).asEagerSingleton()
  }
}
