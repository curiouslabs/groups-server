package com.curiouslabs.groups.controller

import javax.inject.Inject

import com.curiouslabs.authenticator.error.{RegistrationServiceError, UserServiceError}
import com.curiouslabs.authenticator.model.User
import com.curiouslabs.authenticator.repository.UserCredentials
import com.curiouslabs.authenticator.service.{AuthenticationActionService, RegistrationService, UserCreationCommand, UserService}
import com.curiouslabs.authenticator.util.FutureAssist
import com.curiouslabs.groups.error.UserProfileServiceError
import com.curiouslabs.groups.http.serialization.JSONFormats._
import com.curiouslabs.groups.model.UserProfile
import com.curiouslabs.groups.service._
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by visitor15 on 11/7/16.
  */
class UserManagementController @Inject() (authenticationActionService: AuthenticationActionService,
                                          registrationService: RegistrationService,
                                          userService: UserService,
                                          userProfileService: UserProfileService) extends CrudController with GroupInteraction with FutureAssist {

  override def create(): Action[AnyContent] = Action.async { request =>
    val optCreateRequest = request.body.asJson.get.validate[UserCreationCommand].asOpt
    optCreateRequest match {
      case Some(creationRequest)  => createInternal(creationRequest)
      case None                   => Future.successful(Status(412)("User create request not found"))
    }
  }

  override def read(id: String): Action[AnyContent] = authenticationActionService.Authenticated { request =>
    userProfileService.read(UserProfileReadRequest(request.user.id)).map { result =>
      result match {
        case Left(e)            => Status(e.code)(e.message)
        case Right(userProfile) => Ok(Json.toJson(userProfile))
      }
    }
  }

  override def update(id: String): Action[AnyContent] = Action.async { request =>
    Future.successful(Status(500)("Unsupported action"))
  }

  override def delete(id: String): Action[AnyContent] = Action.async { request =>
    Future.successful(Status(500)("Unsupported action"))
  }

  override def join(id: String, groupId: String): Action[AnyContent] = authenticationActionService.Authenticated { request =>
    userProfileService.joinGroup(id, groupId).map { r =>
      r match {
        case Left(e)  => Status(e.code)(e.message)
        case Right(v) => Ok(Json.toJson(v))
      }
    }
  }

  override def leave(id: String, groupId: String): Action[AnyContent] = authenticationActionService.Authenticated { request =>
    userProfileService.leaveGroup(id, groupId).map { r =>
      r match {
        case Left(e)  => Status(e.code)(e.message)
        case Right(v) => Ok(Json.toJson(v))
      }
    }
  }

  override def findJoinedGroups(id: String): Action[AnyContent] = authenticationActionService.Authenticated { request =>
    userProfileService.findJoinedGroups(id).map { r =>
      r match {
        case Left(e)  => Status(e.code)(e.message)
        case Right(v) => Ok(Json.toJson(v))
      }
    }
  }

  private def createInternal(userCreationCommand: UserCreationCommand): Future[Result] = {
    for {
      registrationE <- registrationService.createNewRegistrant(userCreationCommand)
      userE         <- createUserFromRegistrationResult(userCreationCommand, registrationE)
      userProfileE  <- createUserProfileFromUserCreationResult(userCreationCommand, userE)
    } yield userProfileE match {
      case Left(e)  => Status(e.code)(e.message)
      case Right(p) => Created(Json.toJson(p))
    }
  }

  private def createUserFromRegistrationResult(userCreationCommand: UserCreationCommand, registrationE: Either[RegistrationServiceError, UserCredentials]): Future[Either[UserServiceError, User]] = {
    registrationE match {
      case Left(e)  => asFuture(Left(UserServiceError(e.code, e.message)))
      case Right(r) => userService.create(userCreationCommand.copy(id = Some(r.id)))
    }
  }

  private def createUserProfileFromUserCreationResult(userCreationCommand: UserCreationCommand, userE: Either[UserServiceError, User]): Future[Either[UserProfileServiceError, UserProfile]] = {
    userE match {
      case Left(e)  => asFuture[Either[UserProfileServiceError, UserProfile]](Left(UserProfileServiceError(e.code, e.message)))
      case Right(u) => userProfileService.create(UserProfileCreationRequest(userCreationCommand.email, userCreationCommand.password, userCreationCommand.firstName, userCreationCommand.lastName, userCreationCommand.displayName, Option(u.id)))
    }
  }
}
