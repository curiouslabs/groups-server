package com.curiouslabs.groups.controller

import play.api.mvc.{Action, AnyContent}

/**
  * Created by visitor15 on 11/19/16.
  */
trait GroupInteraction {
  def join(id: String, groupId: String): Action[AnyContent]

  def leave(id: String, groupId: String): Action[AnyContent]

  def findJoinedGroups(id: String): Action[AnyContent]
}
