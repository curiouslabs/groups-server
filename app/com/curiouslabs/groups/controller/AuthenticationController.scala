package com.curiouslabs.groups.controller

import javax.inject.Inject

import com.curiouslabs.authenticator.error.UserServiceError
import com.curiouslabs.authenticator.model.{AuthenticationReceipt, User}
import com.curiouslabs.authenticator.repository.UserCredentials
import com.curiouslabs.authenticator.service.{AuthenticationActionService, AuthenticationService, UserReadCommand, UserService}
import com.curiouslabs.authenticator.util.FutureAssist
import com.curiouslabs.groups.http.serialization.JSONFormats._
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, Controller}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by visitor15 on 11/19/16.
  */
class AuthenticationController @Inject()(authenticationActionService: AuthenticationActionService,
                                         authenticationService: AuthenticationService,
                                         userService: UserService) extends Controller with FutureAssist {

  def authenticate(): Action[AnyContent] = authenticationActionService.BasicAuth { request =>
    for {
      optCredentials  <- authenticationService.getCredentialsByUsername(request.usernamePassword.username)
      userE           <- userService.read(UserReadCommand(optCredentials.getOrElse(UserCredentials.emptyCredentials).id))
      response        <- authenticateFromUserResult(userE)
    } yield response match {
      case Left(e)                => Status(e.code)(e.message)
      case Right(authentication)  => Status(200)(Json.toJson(authentication))
    }
  }

  private def authenticateFromUserResult(userE: Either[UserServiceError, User]): Future[Either[UserServiceError, AuthenticationReceipt]] = {
    userE match {
      case Left(e)  => asFuture(Left(e))
      case Right(u) => authenticationService.createNewUserAuthentication(u).map { optAuthReceipt =>
        optAuthReceipt.map { authReceipt =>
          Right(authReceipt)
        }.getOrElse(Left(UserServiceError(500, "Error occurred creating authentication")))
      }
    }
  }
}
