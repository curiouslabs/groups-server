package com.curiouslabs.groups.controller

import play.api.mvc.{Action, AnyContent, Controller}

/**
  * Created by visitor15 on 11/7/16.
  */
trait CrudController extends Controller {
  def create(): Action[AnyContent]
  def read(id: String): Action[AnyContent]
  def update(id: String): Action[AnyContent]
  def delete(id: String): Action[AnyContent]
}