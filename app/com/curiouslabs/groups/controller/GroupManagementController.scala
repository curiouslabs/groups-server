package com.curiouslabs.groups.controller

import javax.inject.Inject

import com.curiouslabs.authenticator.service.AuthenticationActionService
import com.curiouslabs.groups.http.serialization.JSONFormats._
import com.curiouslabs.groups.model.Location
import com.curiouslabs.groups.service.{GroupCreationRequest, GroupReadRequest, GroupService}
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, Result}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by visitor15 on 11/7/16.
  */
class GroupManagementController @Inject() (groupService: GroupService,
                                           authenticationActionService: AuthenticationActionService)  extends CrudController with Search {

  override def create(): Action[AnyContent] = authenticationActionService.Authenticated { request =>
    val optCreateRequest = request.body.asJson.get.validate[GroupCreationRequest].asOpt
    optCreateRequest match {
      case Some(creationRequest)  => createInternal(creationRequest)
      case None                   => Future.successful(PreconditionFailed("User create request not found"))
    }
  }

  override def read(id: String): Action[AnyContent] = authenticationActionService.Authenticated { request =>
    groupService.read(GroupReadRequest(Some(id), None, None, None, None, None)).map { optGroup =>
      optGroup match {
        case Left(e)  => Status(e.code)(e.message)
        case Right(g) => Status(200)(Json.toJson(g))
      }
    }
  }

  override def update(id: String): Action[AnyContent] = Action.async { request =>
    Future.successful(NotImplemented("Unsupported action"))
  }

  override def delete(id: String): Action[AnyContent] = Action.async { request =>
    Future.successful(NotImplemented("Unsupported action"))
  }

  override def search(title:       Option[String],
                      latitude:    Option[Double],
                      longitude:   Option[Double],
                      minDistance: Option[Long],
                      maxDistance: Option[Long],
                      limit:       Option[Long],
                      skip:        Option[Long]): Action[AnyContent] = Action.async { request =>
    val location = if(latitude.isDefined && longitude.isDefined) Some(Location(latitude.get, longitude.get)) else None
    val readRequest = GroupReadRequest(None, location, minDistance, maxDistance, limit, skip)
    searchInternal(readRequest)
  }

  def addEvent(id: String) = Action.async { request =>
    Future.successful(NotImplemented("Unsupported action"))
  }

  private def createInternal(creationRequest: GroupCreationRequest): Future[Result] = {
    groupService.create(creationRequest).map { r =>
      r match {
        case Left(e)  => Status(e.code)(e.message)
        case Right(v) => Ok(Json.toJson(v))
      }
    }
  }

  private def searchInternal(readRequest: GroupReadRequest): Future[Result] = {
    groupService.findNearest(readRequest).map { r =>
      r match {
        case Left(e)  => Status(e.code)(e.message)
        case Right(v) => Ok(Json.toJson(v))
      }
    }
  }
}
