package com.curiouslabs.groups.controller

import java.io.File

import com.curiouslabs.authenticator.service.AuthenticationActionService
import com.google.inject.Inject
import play.api.mvc.{Action, Controller}

/**
  * Created by visitor15 on 1/15/17.
  */
class AssetController @Inject() (authenticationActionService: AuthenticationActionService) extends Controller {

  def upload = Action(parse.temporaryFile) { request =>
    request.body.moveTo(new File("/tmp/picture/uploaded"))
    Ok("File uploaded")
  }
}
