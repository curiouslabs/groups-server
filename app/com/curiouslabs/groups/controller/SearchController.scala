package com.curiouslabs.groups.controller

import play.api.mvc.{Action, AnyContent}

/**
  * Created by visitor15 on 11/14/16.
  */
trait Search {
  def search(title:       Option[String],
             latitude:    Option[Double],
             longitude:   Option[Double],
             minDistance: Option[Long],
             maxDistance: Option[Long],
             limit:       Option[Long],
             skip:        Option[Long]): Action[AnyContent]
}
