package com.curiouslabs.groups.repository

import com.curiouslabs.groups.model.ReferenceType.ReferenceType
import com.curiouslabs.groups.model.Visibility.Visibility
import com.curiouslabs.groups.model._
import reactivemongo.bson.{BSON, BSONHandler, BSONString, Macros}

/**
  * Created by visitor15 on 11/8/16.
  */
object BSONFormats {

  implicit object BSONReferenceTypeEnumHandler extends BSONHandler[BSONString, ReferenceType] {
    def read(doc: BSONString)       = ReferenceType.withName(doc.value)
    def write(stats: ReferenceType) = BSON.write(stats.toString)
  }

  implicit object BSONVisibilityTypeEnumHandler extends BSONHandler[BSONString, Visibility] {
    def read(doc: BSONString)     = Visibility.withName(doc.value)
    def write(stats: Visibility)  = BSON.write(stats.toString)
  }

  implicit val Distance_MacroHandler                  = Macros.handler[Distance]
  implicit val Member_MacroHandler                    = Macros.handler[Member]
  implicit val Location_MacroHandler                  = Macros.handler[Location]
  implicit val Reference_MacroHandler                 = Macros.handler[Reference]
  implicit val User_MacroHandler                      = Macros.handler[PersistedUserProfile]
  implicit val PersistedLocation_MacroHandler         = Macros.handler[PersistedLocation]
//  implicit val PersistedUser_MacroHandler             = Macros.handler[PersistedUser]
  implicit val Group_MacroHandler                     = Macros.handler[PersistedGroup]
//  implicit val PersistedAuthTicket_MacroHandler       = Macros.handler[PersistedAuthTicket]
//  implicit val PersistedUserCredentials_MacroHandler  = Macros.handler[PersistedUserCredentials]
}