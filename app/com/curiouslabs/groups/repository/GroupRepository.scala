package com.curiouslabs.groups.repository

import java.util.UUID
import javax.inject.Inject

import com.curiouslabs.groups.error.GroupRepositoryError
import com.curiouslabs.groups.model.Visibility.Visibility
import com.curiouslabs.groups.model._
import com.curiouslabs.groups.repository.BSONFormats._
import com.curiouslabs.groups.service.{GroupCreationRequest, GroupReadRequest}
import com.curiouslabs.mongo.{CrudRepository, DefaultMongoConfiguration}
import reactivemongo.bson.{BSONDocument, array, document}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by visitor15 on 11/8/16.
  */
class GroupRepository @Inject() (mongoConfig: DefaultMongoConfiguration) extends CrudRepository[PersistedGroup] {
  implicit val groupCollection = mongoConfig.getMongoDataStore("group").collection

  def findGroupById(id: String) = readOne { BSONDocument("_id" -> id) }.map { optGroup => optGroup.map { p => p.toGroup }}
  def findGroupsById(groupIdSet: Set[String]) = readMany() { BSONDocument("_id" -> BSONDocument("$in" -> groupIdSet)) }.map {pGroups => pGroups.map { p => p.toGroup }}

  def createGroup(groupCreationRequest: GroupCreationRequest): Future[Option[Group]] = {
    groupCollection.find(BSONDocument("title" -> groupCreationRequest.title)).one[PersistedGroup].flatMap { groupOpt =>
      groupOpt match {
        case Some(u) => Future.successful(None)
        case _ => {
          val persistedGroup = createNewPersistedGroup(groupCreationRequest)
          groupCollection.insert(persistedGroup).map { wr =>
            wr.ok match {
              case true => Some(persistedGroup.toGroup)
              case _    => None
            }
          }
        }
      }
    }
  }

  def updateGroup(group: Group): Future[Option[Group]] = {
    groupCollection.update(BSONDocument("_id" -> group.id), BSONDocument("$set" -> PersistedGroup.fromGroup(group)), upsert = false, multi = false).map { updateWriteResult =>
      updateWriteResult.ok match {
        case true => Some(group)
        case _    => None
      }
    }
  }

  def findNearest(groupReadRequest: GroupReadRequest): Future[Option[List[Group]]] = {
    import groupCollection.BatchCommands.AggregationFramework.GeoNear

    groupReadRequest.location match {
      case Some(loc)  => {
        groupCollection.aggregate(GeoNear(document(
          "type" -> "Point",
          "coordinates" -> array(loc.latitude, loc.longitude)
        ), distanceField  = Some("distance.calculated"),
          minDistance     = groupReadRequest.minDistance,
          maxDistance     = groupReadRequest.maxDistance,
          query           = Some(document("visibility" -> "PUBLIC")),
          limit           = groupReadRequest.limit.getOrElse(25),
          spherical       = true)).map { foundDoc => Option(foundDoc.head[PersistedGroup].map(pGroup => pGroup.toGroup)) }
      }
      case None       => Future.successful(None)
    }
  }

  def addUser(userId: String, group: Group): Future[Either[GroupRepositoryError, Group]] = {
    val isFounder = group.founders.filter(userRef => userRef.id == userId).size > 0
    val isMember  = group.members.map(members => members.filter(userRef => userRef.id == userId).size > 0).getOrElse(false)
    if(isFounder)     Future.successful(Left(GroupRepositoryError(406, s"User $userId is the founder for group ${group.title}")))
    else if(isMember) Future.successful(Left(GroupRepositoryError(406, s"User $userId is already a member of ${group.title}")))
    else {
      // Add user to group and persist
      updateGroup(group.addMember(ReferenceFactory.createUserReference(userId))).map { optGroup =>
        optGroup match {
          case Some(g)  => Right(g)
          case None     => Left(GroupRepositoryError(500, s"An error occurred updating group ${group.id}"))
        }
      }
    }
  }

  def removeUser(userId: String, group: Group): Future[Either[GroupRepositoryError, Group]] = {
    val isFounder = group.founders.filter(userRef => userRef.id == userId).size > 0
    val isMember  = group.members.map(members => members.filter(userRef => userRef.id == userId).size > 0).getOrElse(false)
    if(!isFounder && !isMember) Future.successful(Left(GroupRepositoryError(406, s"User $userId is not a member of ${group.title}")))
    else {
      updateGroup(group.removeMember(ReferenceFactory.createUserReference(userId))).map { optGroup =>
        optGroup match {
          case Some(g)  => Right(g)
          case None     => Left(GroupRepositoryError(500, s"An error occurred updating group ${group.id}"))
        }
      }
    }
  }

  private def createNewPersistedGroup(groupCreationRequest: GroupCreationRequest): PersistedGroup = {
    val location = List(groupCreationRequest.location.latitude, groupCreationRequest.location.longitude)
    val persistedLocation = PersistedLocation("Point", location)
    PersistedGroup(UUID.randomUUID().toString,
      System.currentTimeMillis(),
      System.currentTimeMillis(),
      persistedLocation,
      groupCreationRequest.visibility,
      groupCreationRequest.title,
      groupCreationRequest.description,
      groupCreationRequest.founders,
      groupCreationRequest.members)
  }
}

case class PersistedGroup(_id: String,
                          creationDate: Long,
                          lastUpdated:  Long,
                          location:     PersistedLocation,
                          visibility:   Visibility,
                          title:        String,
                          description:  String,
                          founders:     Set[Reference],
                          members:      Option[Set[Reference]],
                          distance:     Option[Distance] = None) {
  def toGroup: Group = Group(_id, creationDate, lastUpdated, location.toLocation, visibility, title, description, founders, members, distance)
}

object PersistedGroup {
  def fromGroup(group: Group): PersistedGroup = {
    val location = List(group.location.latitude, group.location.longitude)
    PersistedGroup(group.id, group.creationDate, group.lastUpdated, PersistedLocation("Point", location), group.visibility, group.title, group.description, group.founders, group.members)
  }
}

// GeoJSON Point object - http://geojson.org/geojson-spec.html#point
case class PersistedLocation(`type`: String, coordinates: List[Double]) {
  def toLocation = Location(coordinates.apply(0), coordinates.apply(1))
}