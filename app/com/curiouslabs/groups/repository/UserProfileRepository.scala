package com.curiouslabs.groups.repository

import javax.inject.Inject

import com.curiouslabs.groups.model.{Reference, UserProfile}
import com.curiouslabs.groups.repository.BSONFormats._
import com.curiouslabs.mongo.{CrudRepository, DefaultMongoConfiguration}
import reactivemongo.bson.BSONDocument

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by visitor15 on 11/8/16.
  */
class UserProfileRepository @Inject()(mongoConfig: DefaultMongoConfiguration) extends CrudRepository[PersistedUserProfile] {
  implicit val userCollection = mongoConfig.getMongoDataStore("userProfile").collection

  private def readOneInternal(bSONDocument: BSONDocument) = readOne { bSONDocument }.map { optPersistedUserProfile => optPersistedUserProfile.map { p => p.toUserProfile } }

  def saveUser(userProfile: UserProfile) = save(PersistedUserProfile.fromUserProfile(userProfile)) { BSONDocument("_id" -> userProfile.id) }
  def findUserById(id: String) = readOneInternal(BSONDocument("_id" -> id))
  def findUserByEmail(email: String) = readOneInternal(BSONDocument("email" -> email))
}

case class PersistedUserProfile(_id: String,
                                creationDate:  Long,
                                lastUpdated:   Long,
                                firstName:     String,
                                lastName:      String,
                                email:         String,
                                displayName:   Option[String],
                                groups:        Option[Set[Reference]] = None) {
  def toUserProfile: UserProfile = UserProfile(_id, creationDate, lastUpdated, firstName, lastName, email, displayName, groups)
}

object PersistedUserProfile {
  def fromUserProfile(user: UserProfile) = PersistedUserProfile(user.id, user.creationDate, user.lastUpdated, user.firstName, user.lastName, user.email, user.displayName, user.groups)
}
