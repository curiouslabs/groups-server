package com.curiouslabs.groups.error

/**
  * Created by visitor15 on 11/7/16.
  */
trait ServiceError {
  def code: Int
  def message: String
}

trait RepositoryError {
  def code: Int
  def message: String
}

case class GroupServiceError(code: Int, message: String) extends ServiceError
case class UserProfileServiceError(code: Int, message: String) extends ServiceError
case class GroupRepositoryError(code: Int, message: String) extends RepositoryError