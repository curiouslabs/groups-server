package com.curiouslabs.groups.model

import com.curiouslabs.groups.service.UserProfileCreationRequest

/**
  * Created by visitor15 on 11/7/16.
  */
case class UserProfile(id:           String,
                       creationDate: Long,
                       lastUpdated:  Long,
                       firstName:    String,
                       lastName:     String,
                       email:        String,
                       displayName:  Option[String],
                       groups:       Option[Set[Reference]] = None) {
  def addGroup(ref: Reference)    = this.copy(this.id, this.creationDate, System.currentTimeMillis, this.firstName, this.lastName, this.email, this.displayName, Option(groups.getOrElse(Set[Reference]()) + ref))
  def removeGroup(ref: Reference) = this.copy(this.id, this.creationDate, System.currentTimeMillis, this.firstName, this.lastName, this.email, this.displayName, Option(groups.getOrElse(Set[Reference]()).filterNot(r => r.id == ref.id)))
}

object UserProfile {
  def fromCreationRequest(userCreationRequest: UserProfileCreationRequest) =
    UserProfile(userCreationRequest.id.get,
      System.currentTimeMillis,
      System.currentTimeMillis,
      userCreationRequest.firstName,
      userCreationRequest.lastName,
      userCreationRequest.email,
      userCreationRequest.displayName)
}