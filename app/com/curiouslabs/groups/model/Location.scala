package com.curiouslabs.groups.model

/**
  * Created by visitor15 on 11/7/16.
  */
case class Location(latitude: Double, longitude: Double)

case class Distance(calculated: Double)