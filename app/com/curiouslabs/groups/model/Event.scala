package com.curiouslabs.groups.model

/**
  * Created by visitor15 on 11/14/16.
  */
case class Event(id:          String,
                 title:       String,
                 description: String,
                 startDate:   Long,
                 endDate:     Option[Long],
                 repeats:     Option[Repeat],
                 reference:   Reference)