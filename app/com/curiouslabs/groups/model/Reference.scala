package com.curiouslabs.groups.model

import com.curiouslabs.groups.model.ReferenceType.ReferenceType

/**
  * Created by visitor15 on 11/14/16.
  */
case class Reference(id: String, referenceType: ReferenceType)

object ReferenceType extends Enumeration {
  type ReferenceType  = Value
  val GROUP           = Value("GROUP")
  val USER            = Value("USER")
  val MEMBER          = Value("MEMBER")
}

object ReferenceFactory {
  def createUserReference(userId: String)   = Reference(userId, ReferenceType.USER)
  def createGroupReference(groupId: String) = Reference(groupId, ReferenceType.GROUP)
}