package com.curiouslabs.groups.model

import com.curiouslabs.groups.model.Repeat.Frequency
import com.curiouslabs.groups.model.TimeFrequency.TimeFrequency

/**
  * Created by visitor15 on 11/14/16.
  */
case class Repeat(frequency:      Frequency = 1,
                  timeFrequency:  TimeFrequency,
                  timeType:       Option[TimeType] = None,
                  startTime:      Long,
                  endTime:        Option[Long] = None)

object Repeat {
  type Frequency = Int
}

object TimeFrequency extends Enumeration {
  type TimeFrequency  = Value
  val HOUR            = Value("HOUR")
  val DAY             = Value("DAY")
  val WEEK            = Value("WEEK")
  val MONTH           = Value("MONTH")
  val YEAR            = Value("YEAR")
}

trait TimeType extends Enumeration

object Day extends TimeType {
  type Day      = Value
  val MONDAY    = Value("MONDAY")
  val TUESDAY   = Value("TUESDAY")
  val WEDNESDAY = Value("WEDNESDAY")
  val THURSDAY  = Value("THURSDAY")
  val FRIDAY    = Value("FRIDAY")
  val SATURDAY  = Value("SATURDAY")
  val SUNDAY    = Value("SUNDAY")
}

object Month extends TimeType {
  type Month    = Value
  val JANUARY   = Value("JANUARY")
  val FEBRUARY  = Value("FEBRUARY")
  val MARCH     = Value("MARCH")
  val APRIL     = Value("APRIL")
  val MAY       = Value("MAY")
  val JUNE      = Value("JUNE")
  val JULY      = Value("JULY")
  val AUGUST    = Value("AUGUST")
  val SEPTEMBER = Value("SEPTEMBER")
  val OCTOBER   = Value("OCTOBER")
  val NOVEMBER  = Value("NOVEMBER")
  val DECEMBER  = Value("DECEMBER")
}