package com.curiouslabs.groups.model

import com.curiouslabs.groups.model.Visibility.Visibility

/**
  * Created by visitor15 on 11/7/16.
  */
case class Group(id:            String,
                 creationDate:  Long,
                 lastUpdated:   Long,
                 location:      Location,
                 visibility:    Visibility,
                 title:         String,
                 description:   String,
                 founders:      Set[Reference],
                 members:       Option[Set[Reference]],
                 distance:      Option[Distance] = None) {
  def addMember(ref: Reference)     = this.copy(this.id, this.creationDate, System.currentTimeMillis, this.location, this.visibility, this.title, this.description, this.founders, Option(members.getOrElse(Set[Reference]()) + ref))
  def removeMember(ref: Reference)  = this.copy(this.id, this.creationDate, System.currentTimeMillis, this.location, this.visibility, this.title, this.description, this.founders, Option(members.getOrElse(Set[Reference]()).filterNot(r => r.id == ref.id)))
}

object Visibility extends Enumeration {
  type Visibility = Value

  val PRIVATE = Value("PRIVATE")
  val PUBLIC  = Value("PUBLIC")
}