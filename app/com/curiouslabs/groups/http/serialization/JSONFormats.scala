package com.curiouslabs.groups.http.serialization

import com.curiouslabs.authenticator.model.AuthenticationReceipt
import com.curiouslabs.authenticator.service.UserCreationCommand
import com.curiouslabs.groups.model._
import com.curiouslabs.groups.service._
import com.curiouslabs.groups.util.EnumUtil
import play.api.libs.json._

/**
  * Created by visitor15 on 11/12/16.
  */
object JSONFormats {
  implicit val visibilityTypeEnumMacro    = EnumUtil.enumFormat(Visibility)
  implicit val referenceTypeEnumMacro     = EnumUtil.enumFormat(ReferenceType)
  implicit val referenceJsonMacro         = Json.format[Reference]
  implicit val distanceJsonMacro          = Json.format[Distance]
  implicit val locationJsonMacro          = Json.format[Location]
  implicit val groupJsonMacro             = Json.format[Group]
  implicit val userJsonMacro              = Json.format[UserProfile]

  implicit val userCreateRequestRead      = Json.reads[UserProfileCreationRequest]
  implicit val userReadRequestRead        = Json.reads[UserProfileReadRequest]
  implicit val userUpdateRequestRead      = Json.reads[UserProfileUpdateRequest]
  implicit val userDeleteRequestRead      = Json.reads[UserProfileDeleteRequest]

  implicit val userCreationCommandMacro   = Json.format[UserCreationCommand]

  implicit val groupCreateRequestRead     = Json.reads[GroupCreationRequest]
  implicit val groupReadRequestRead       = Json.reads[GroupReadRequest]
  implicit val groupUpdateRequestRead     = Json.reads[GroupUpdateRequest]
  implicit val groupDeleteRequestRead     = Json.reads[GroupDeleteRequest]

  implicit val authenticationReceiptWrite = Json.writes[AuthenticationReceipt]
}
