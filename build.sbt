name := "groups-server"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

version := "0.1-SNAPSHOT"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  cache,
  ws,
  "com.typesafe.akka" %% "akka-osgi"  % "2.4.12",
  "org.reactivemongo" %% "reactivemongo" % "0.12.0",
  "com.curiouslabs"   %% "mongo-datastore"  % "0.1-SNAPSHOT" changing(),
  "com.curiouslabs"   %% "authenticator"    % "0.1-SNAPSHOT" changing()
)

resolvers ++= {
  Seq(
    Resolver.jcenterRepo,
    "Typesafe repository" at "https://repo.typesafe.com/typesafe/maven-releases/",
    "scalaz-bintray"      at "http://dl.bintray.com/scalaz/releases"
  )
}

cleanFiles <+= target { dir => dir / "universal" }